from bluesky.callbacks.zmq import RemoteDispatcher
d = RemoteDispatcher('localhost:5578')
d.subscribe(print)

# when done subscribing things and ready to use:
d.start()  # runs event loop forever