## Bluesky Tutorial Notebooks

This repository contains a collection of Notebooks containing reference and tutorial material. 

For now you will need to make a new python environment and install the packages defined in requirements.txt

```
python3 -m pip install -r requirements.txt
``` 
soon we will make a container that has the environment already installed

| Notebook    | Description |
| -------- | ------- |
| LaptopCamera | Acquiring images from an arbitary non-epics device. Explores the document model and saving by reference  |
